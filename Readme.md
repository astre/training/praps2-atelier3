<p><img src="https://gitlab.cirad.fr/astre/training/praps2-atelier3/-/raw/main/src/img/flags-praps.png" width="600"></p>
<p><img src="https://gitlab.cirad.fr/astre/training/praps2-atelier3/-/raw/main/src/img/band_praps.png" width="600"></p>

# Troisième atelier régional du projet [PRAPS II](https://praps2.mr/)

Lomé - Togo. 29 septembre au 4 octobre 2024.


Traitement des résultats des enquêtes de séromonitoring des campagnes de
vaccination contre la péri-pneumonie contagieuse bovine (PPCB).


### Présentations

- [Rappel protocole PPCB](https://gitlab.cirad.fr/astre/training/praps2-atelier3/-/raw/main/src/20240927_Rappel_Protocol_PPCB_VF.pptx)

- [Rappel des concepts de gestion et structuration des données](https://astre.gitlab.cirad.fr/training/praps2-atelier3/0_gestion-donnees.html)

- [Préparation des données pour leur analyse](https://astre.gitlab.cirad.fr/training/praps2-atelier3/1_preparation-donnees.html)

- [Analyse des résultats sérologiques](https://astre.gitlab.cirad.fr/training/praps2-atelier3/2_analyse-resultats.html)


### Matériaux supplémentaires

- Sites web du [premier](https://umr-astre.pages.mia.inra.fr/training/praps2-atelier1) et [deuxième](https://umr-astre.pages.mia.inra.fr/training/praps2-atelier2) ateliers

- [Données fictives sur pays fictif](https://astre.gitlab.cirad.fr/training/praps2-atelier3/fake-data.zip)

- Cartographie et données fictives 
[[BFA](https://gitlab.cirad.fr/astre/training/praps2-atelier3/-/raw/main/data/bfa_fakedata.zip)] 
[[MLI](https://gitlab.cirad.fr/astre/training/praps2-atelier3/-/raw/main/data/mli_fakedata.zip)] 
[[MRT](https://gitlab.cirad.fr/astre/training/praps2-atelier3/-/raw/main/data/mrt_fakedata.zip)] 
[[NER](https://gitlab.cirad.fr/astre/training/praps2-atelier3/-/raw/main/data/ner_fakedata.zip)] 
[[SEN](https://gitlab.cirad.fr/astre/training/praps2-atelier3/-/raw/main/data/sen_fakedata.zip)] 
[[TCD](https://gitlab.cirad.fr/astre/training/praps2-atelier3/-/raw/main/data/tcd_fakedata.zip)]

- [Vidéo-tutoriel d'analyse en Excel](https://gitlab.cirad.fr/astre/training/praps2-atelier3/-/raw/main/assets/Tutoriel%20traitement%20enqu%C3%AAte%20PPCB%20def.mp4?ref_type=heads&inline=false)

- [Script d'analyse en R](https://gitlab.cirad.fr/astre/training/praps2-atelier3/-/raw/main/src/analyse_PPCB.R?ref_type=heads)

- Modèle de rapport descriptif et d'analyse [[code](https://gitlab.cirad.fr/astre/training/praps2-atelier3/-/raw/main/src/modele-rapport.qmd?ref_type=heads)] [[PDF](modele-rapport.pdf)]

### Documents de référence

- IDvet kit cELISA PPCB: [insert (protocole du kit)](https://gitlab.cirad.fr/astre/training/praps2-atelier3/-/raw/main/assets/Insert_CBPPC_ver0623_FR_doc14841.pdf) et [Rapport de validation du kit](https://gitlab.cirad.fr/astre/training/praps2-atelier3/-/raw/main/assets/DP_CBPPC_ver0623_EN_doc1278.pdf)

- Docs CIRAD pour les analyses avec ce kit: [Feuille de travail](https://gitlab.cirad.fr/astre/training/praps2-atelier3/-/raw/main/assets/E-TE-12 feuille de travail et calculs cELISA PPCB (ID).xlsx) et [calculs & Carte de contrôles](https://gitlab.cirad.fr/astre/training/praps2-atelier3/-/raw/main/assets/E-TE- 46 Carte de contrôles cELISA PPCB Kit ID.xlsx)

- Formule de calcul des [intervalles de confiance des proportions](https://umr-astre.pages.mia.inra.fr/training/praps2-atelier2/analyse-resultats.html#/taux-de-communes-prot%C3%A9g%C3%A9es-par-r%C3%A9gion)


### Liens utiles

Formations en ligne, gratuites, en français, en autonomie, à votre rythme

- [Introduction à R pour l'Analyse Qualitative et Cartographique du Risque](https://gitlab.cirad.fr/astre/training/praps2-atelier3/-/raw/main/assets/E-TE-12 feuille de travail et calculs cELISA PPCB (ID).xlsx) et [calculs & Carte de contrôles](https://gitlab.cirad.fr/astre/training/praps2-atelier3/-/raw/main/assets/R-intro.zip)

- [Initiez-vous à R pour l'analyse de données](https://openclassrooms.com/fr/courses/8248096-initiez-vous-a-r-pour-lanalyse-de-donnees) (6h)

- [Nettoyez et analysez votre jeu de données](https://openclassrooms.com/en/courses/7410486-nettoyez-et-analysez-votre-jeu-de-donnees) (10h)

Livres de référence en ligne

- C. Ismay & A. Y. Kim (2024) Statistical Inference via Data Science [Ch. 1 Getting Started with Data in R](https://moderndive.netlify.app/1-getting-started.html)

- Serge-Étienne Parent (2020) [Analyse et modélisation d’agroécosystèmes](https://essicolo.github.io/ecologie-mathematique-R/index.html)

  - Peut servir comme intro à R, importation et traitement basique de données (Ch. 2-3), visualisation (Ch. 4)

- Julie Lowndes & Allison Horst (2021) [R for Excel Users](https://rstudio-conf-2020.github.io/r-for-excel/)  

  - Introduction to R, RStudio, RMarkdown, GitHub, Visualisation with `ggplot2` and data manipulation with `tidyr` and `dplyr`

